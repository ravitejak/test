#Installation instructions

Download this repo from bitbucket 

`git clone git@bitbucket.org:ravitejak/test.git`

Run this to install all the node dependecies

`npm install`

Fill the mongo db server connection details in 

`config/mongo.js`

Then run this to start the server 

`npm start`

and then check here for API Documentation 

[API DOC](https://documenter.getpostman.com/view/39643/node-api/716dFWw)

check `config/users.js` to edit user credentials

bydefault server runs on port number 8000 , so after running server visit localhost:8000 .
