var mongoose = require('mongoose');
var Boom=require('boom');
var Joi=require('joi');
var mongo_config=require('../config/mongo');

mongoose.connect(mongo_config.conn_string);
productModel = mongoose.model('Products',{
  product_id:Number,
  product_name:String,
  product_desc:String,
  product_url:String,
  product_price:Number
});


let products={};


products.getList=function(request,response){
    productModel.find({},function(err,product){
      if (err) {
          response(Boom.badImplementation('Error while retrieving data',err));
      }
      response({error:false,data:product});
    });

}
products.getListConfig={auth: 'simple'}

products.getProduct=function(request,response){
  productModel.findById(request.params.product_id, function(err, product) {
    if (err) {
        return response(Boom.badRequest('Error while retrieving data',err));
    }
    if(product==null){
      // return response
      return response(Boom.badRequest('No Record with the given ID',err));
    }
    return response({error:false,data:product});
  });
}
products.getProductConfig={auth: 'simple'}

products.createProduct=function(request,response){
  var new_product = new productModel(request.payload);
  new_product.save(function(err, product) {
    if (err)
      response(Boom.badRequest('Error while Creating Product',err));
    response({error:false,data:product});
  });
}
products.createProductConfig={
      validate: {
          payload: {
              product_id:Joi.number().required(),
              product_name:Joi.string().min(3).max(10).required(),
              product_desc:Joi.string().required(),
              product_url:Joi.string(),
              product_price:Joi.number().required()
          }
      },
      auth: 'simple'
}


products.updateProduct = function(request, response) {
  productModel.findOneAndUpdate({_id: request.params.product_id}, request.payload, {new: true}, function(err, product) {
    if (err) {
        return response(Boom.badRequest('Error while Updating Product',err));
    }
    return response({error:false,data:product});
  });
};
products.updateProductConfig={
      validate: {
          payload: {
              product_id:Joi.number().required(),
              product_name:Joi.string().min(3).max(10).required(),
              product_desc:Joi.string().required(),
              product_url:Joi.string(),
              product_price:Joi.number().required()
          },
          params:{
            product_id:Joi.string().required()
          }
      },
      auth: 'simple'
}

products.deleteProduct = function(request, response) {
  productModel.remove({
    _id: request.params.product_id
  }, function(err, product) {
    if (err)
        return response(Boom.badRequest('Error while Deleting Product',err));

    if(product.result.n==0)
      return response({ error:false,message: "Product Doesn't exist or already deleted" });

    return response({ error:false,message: 'Product successfully deleted' });
  });
};

products.deleteProductConfig={
      validate: {
          params:{
            product_id:Joi.string().required()
          }
      },
      auth: 'simple'
}


module.exports=products;
