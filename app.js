'use strict';

const Hapi = require('hapi');
const routes = require('./config/routes');

const Bcrypt = require('bcrypt');

const users = require('./config/users');

const validate = function (request, username, password, callback) {

    const user = users[username];
    if (!user) {
        return callback(null, false);
    }

    Bcrypt.compare(password, user.password, (err, isValid) => {

        callback(err, isValid, { id: user.id, name: user.name });
    });
};

// Create a server with a host and port
const server = new Hapi.Server();
server.connection({
    host: 'localhost',
    port: 8000
});





server.register(require('hapi-auth-basic'), (err) => {
    server.auth.strategy('simple', 'basic', { validateFunc: validate });
    // Add the route
    for (let i = 0; i < routes.length; i++) {
      server.route(routes[i]);
    }
});




// Start the server
server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});
