const products = require('../controllers/products');


let routes=[
  {
      method: ['GET'],
      path:'/products',
      handler: products.getList,
      config: products.getListConfig
  },
  {
      method: ['POST'],
      path:'/products',
      handler: products.createProduct,
      config: products.createProductConfig
  },
  {
      method: ['GET'],
      path:'/products/{product_id}',
      handler: products.getProduct,
      config: products.getProductConfig

  },
  {
      method: ['POST','PUT'],
      path:'/products/{product_id}',
      handler: products.updateProduct,
      config: products.updateProductConfig
  },
  {
      method: ['DELETE'],
      path:'/products/{product_id}',
      handler: products.deleteProduct,
      config: products.deleteProductConfig
  }
]

module.exports = routes;
